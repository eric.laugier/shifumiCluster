from threading import Thread, currentThread
import urllib.request
from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn
import time
import json
from random import randint
import requests
from apscheduler.schedulers.background import BackgroundScheduler
from pprint import pprint

confPathFile = "node.conf.json"


def loadConfigurationFile(confPathFile):
    file = open(confPathFile)
    configuration = json.load(file)
    return configuration


conf = loadConfigurationFile(confPathFile)

try:
    witness = bool(conf["isWitness"])
except:
    witness = False

try:
    other_node = conf["nodePair"]
    bindings = conf["bindings"]
    port = int(conf["port"])
except:
    raise Exception()

try:
    ippon = bool(conf['ippon'])
except:
    ippon = False

stop: bool = False
isMaster: bool = False
shifumi = ["shi", "fu", "mi"]


class Pong(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(
            bytes('{"threadName":"' + currentThread().getName() + '";"isMaster":"' + str(isMaster) + '"}', 'utf-8'))
        return

    def do_POST(self):
        myVote = shifumi[randint(0, shifumi.__len__()-1)]
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length)
        result = json.loads(post_data)
        otherVote = result['shifumi']

        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(
            bytes('{"shifumi":"' + myVote + '"}', 'utf-8'))
        if myVote == otherVote:
            isMaster = False
        elif otherVote == "ippon":
            isMaster = False
        elif (myVote == "mi" and otherVote == "fu") \
                or (myVote == "fu" and otherVote == "shi") \
                or (myVote == "shi" and otherVote == "mi"):
            isMaster = True
        else:
            isMaster = False

        return


class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """ multithreading the request execution """


class HearthbeatListener(Thread):
    def __init__(self, bindings, port):
        self.bindings = bindings
        self.port = port
        Thread.__init__(self)

    def run(self):
        try:
            with ThreadedHTTPServer((self.bindings, self.port), Pong) as httpd:
                print("serving at port", port)
                httpd.serve_forever()
        except KeyboardInterrupt:
            print('^C received, shutting down the web server')
            httpd.socket.close()
            stop = True


def isOtherNodeMaster(node, port):
    try:
        contents = urllib.request.urlopen(
            "http://" + other_node + ":" + port + "/").read()
        if 'master' not in contents:
            print("other node says that it's not the master")
            return False
        else:
            print("other node says that it's the master")
            return True
    except:
        print("other node seems to be down")
        return False


def doShifumi(node: str, port: int):
    try:
        if ippon:
            myVote = "ippon"
        else:
            myVote = shifumi[randint(0, shifumi.__len__())]
        r = requests.post("http://" + node + ":" + port +
                          "/", data={'shifumi': myVote})
        print("result = " + r.text)
        result = json.loads(r.text)
        otherVote = result['shifumi']
        if myVote == otherVote:
            if ippon:
                return False
            else:
                return doShifumi(node, port)
        elif ippon:
            return True
        elif (myVote == "mi" and otherVote == "fu") \
                or (myVote == "fu" and otherVote == "shi") \
                or (myVote == "shi" and otherVote == "mi"):
            return True
        else:
            return False
    except:
        print("other node seems to be down")
        return True


if isOtherNodeMaster(other_node, port):
    isMaster = False
else:
    isMaster = True

hbs = HearthbeatListener(bindings, port)

hbs.start()

# todo: read https://apscheduler.readthedocs.io/en/latest/userguide.html
scheduler = BackgroundScheduler()

while not stop:
    time.sleep(1)
    if isOtherNodeMaster(other_node, port):
        if(isMaster):
            isMaster = False
            if(doShifumi(other_node, port)):
                isMaster = True
            else:
                isMaster = False

exit(0)
